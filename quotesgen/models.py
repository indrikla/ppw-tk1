from django.db import models

# Create your models here.
class Quote(models.Model):
    quote = models.TextField('Quote', max_length=200, null=True, blank=False)

    def __str__(self):
        return f'{self.quote}'

    class Meta:
        db_table = 'quote'