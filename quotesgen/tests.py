from django.test import TestCase
from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from django.http import HttpRequest
from .models import *
from .views import *

# Create your tests here.
class QuotesGenUnitTest(TestCase):
    def test_activity_url_data_is_exist(self):
        response = Client().get("/quotes/")
        self.assertEquals(200, response.status_code)
    
    def test_template_form(self):
        response = Client().get("/quotes/")
        self.assertTemplateUsed(response, "quotes_gen.html")

    def test_model_create(self):
        contoh = Quote.objects.create(quote="if you think you can you can")
        jumlah = Quote.objects.all().count()
        self.assertEqual(jumlah, 1)

    def test_model_str(self):
        contoh = Quote.objects.create(quote="restyour")
        self.assertEqual("restyour", str(contoh))
