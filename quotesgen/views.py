from django.shortcuts import render
from .models import Quote
from django.db.models import Max
import random


# Create your views here.
def quotesgen(request):
    if request.method == 'POST':
        max_id = Quote.objects.all().aggregate(max_id=Max("id"))['max_id']
        pk = random.randint(29, max_id)
        return render(request, 'quotes_gen.html', {'quotefinal': Quote.objects.get(pk=pk)})
    else:
        return render(request, 'quotes_gen.html')