from django import forms
from .models import Activity

class ActivityForm(forms.Form):
    activity = forms.CharField(max_length=100)