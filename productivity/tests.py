from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Activity

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.activity_url = reverse("productivity:activity")
        self.test_activity = Activity.objects.create(
            activity = "belajar"
        )

    def test_activity_GET(self):
        response = self.client.get(self.activity_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "tracker.html")

    def test_konten(self):
        response = self.client.get(self.activity_url)
        self.assertContains(response, "to-do")
        self.assertContains(response, "feel better")
        self.assertContains(response, "healing")
        self.assertContains(response, "sharing")
        self.assertContains(response, "wonderful day")

    def test_activity_POST(self):
        response = self.client.post(self.activity_url, {
            "activity" : "tidur"
        }, follow=True)
        self.assertContains(response, "tidur")

    def test_notValid_POST(self):
        response = self.client.post(self.activity_url, {
            "aktivitas" : "haha",
        }, follow=True)
        self.assertContains(response, "Wrong input!")  
    