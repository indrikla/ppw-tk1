from django.urls import path
from .views import activity

app_name = 'productivity'

urlpatterns = [
    path('productivity-tracker/', activity, name='activity'),
]