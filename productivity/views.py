from django.shortcuts import render, redirect
from .models import Activity
from .forms import ActivityForm
from django.contrib import messages

def activity(request):
    form = ActivityForm()
    if request.method == 'POST':
        form = ActivityForm(request.POST)
        if form.is_valid():
            Activity.objects.create(**form.cleaned_data)
            messages.success(request, (f"Activity '{request.POST['activity']}' was successfully added !"))
            return redirect('productivity:activity')
        else:
            messages.warning(request, (f"Wrong input!"))
            return redirect('productivity:activity')
    context = {
        'form' : form,
        'activities' : Activity.objects.all()
    }
    return render(request, "tracker.html", context)