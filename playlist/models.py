from django.db import models

# Create your models here.


class Comment(models.Model):
    name = models.CharField(max_length=100, blank=False, null=True)
    comment = models.CharField(max_length=150, blank=False, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'comment'
