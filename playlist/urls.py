from django.urls import path
from . import views

app_name = 'playlist'

urlpatterns = [
    path('', views.playlist, name='playlist'),
]