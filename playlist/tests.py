from django.test import TestCase
from .models import Comment
from . import views
from django.test import Client
from django.urls import reverse

# Create your tests here.
class ModelTest(TestCase):
    def setUp(self):
        self.comment = Comment.objects.create(name="Andin", comment="Saya suka playlistnya.")

    def test_instance_created(self):
        self.assertEqual(Comment.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.comment), "Andin")

    def test_database_field(self):
        self.assertEqual(self.comment.comment, "Saya suka playlistnya.")

class ViewTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.comment = Comment.objects.create(name="Andin", comment="Saya tidak suka playlistnya.")
        self.commentUrl = reverse("playlist:playlist")

    def test_testimoni_url_status(self):
        response = self.client.get(self.commentUrl)
        self.assertEqual(response.status_code, 200)

    def test_post_tambah_testimoni(self):
        response = self.client.post(self.commentUrl, {
            "name":"Andin",
            "comment":"Lagu-lagunya bagus"
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Andin")

