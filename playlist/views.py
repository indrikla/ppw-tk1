from django.shortcuts import render, redirect
from .models import Comment
from .forms import CommentForm
from django.http import HttpResponseRedirect
# Create your views here.


def playlist(request):
    if request.POST:
        form = CommentForm(request.POST)
        if form.is_valid:
            form.save()
            form = CommentForm()
            return HttpResponseRedirect('/playlist/')
    else:
        form = CommentForm()
        comments = Comment.objects.all()
        context = {
            'comments':comments,
            'form':form
        }
        return render(request, 'playlist.html', context)
