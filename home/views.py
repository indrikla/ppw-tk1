from django.shortcuts import render, redirect
from .models import Name
from .forms import NameForm
from django.http import HttpResponse
# Create your views here.


def home(request):
    save = "Someone"
    if request.method == 'POST':
        form = NameForm(request.POST)
        if form.is_valid():
            save = form.save()
    context = {
        'name': save
    }
    return render(request, 'home.html', context)


def landingPage(request):
    form = NameForm()
    context = {
        'form': form,
    }
    return render(request, 'landingPage.html', context)
