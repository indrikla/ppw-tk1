from django.forms import ModelForm
from .models import Name
from django import forms


class NameForm(forms.ModelForm):
    #    name = forms.CharField(label='name', max_length=30, required=True)
    name = forms.CharField(widget=forms.Textarea, label='', required=True)

    class Meta:
        model = Name
        fields = ['name']
