from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.landingPage, name='landingPage'),
    path('home/', views.home, name='home')
]
