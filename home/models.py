from django.db import models

# Create your models here.


class Name(models.Model):
    name = models.CharField(max_length=100, blank=False, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'name'
