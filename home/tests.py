from django.test import TestCase, Client
from home.models import Name
from django.urls import reverse, resolve
from .forms import NameForm

# Create your tests here.


class HomeTest(TestCase):

    def setUp(self):
        self.client = Client()

    def test_model_str(self):
        example = Name.objects.create(name="rikarika")
        self.assertEqual("rikarika", str(example))

    def test_model_create(self):
        Name.objects.create(name="rikacantikkkk")
        Name.objects.create(name="rikakerennn")
        Name.objects.create(name="rikaari")
        count = Name.objects.all().count()
        self.assertEqual(count, 3)

    def test_landing_page_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'landingPage.html')

    def test_landing_page_url(self):
        response = Client().get('')
        self.assertEquals(response.status_code, 200)

    def test_landing_page_content(self):
        response = Client().get('')
        self.assertIn("name", response.content.decode('utf8'))

    def test_home_template(self):
        response = Client().get('/home/')
        self.assertTemplateUsed(response, 'home.html')

    def test_home_url(self):
        response = Client().get('/home/')
        self.assertEquals(response.status_code, 200)

    def test_form_validation_accepted(self):
        form = NameForm(data={'name': 'rikaaa'})
        self.assertTrue(form.is_valid())
